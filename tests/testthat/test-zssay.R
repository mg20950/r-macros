test_that("Success run", {
  ncol = 3
  df2 = "Total no of columns in mtcars are 3" 
  expect_message(zssay("Total no of columns in mtcars are", ncol),df2)
})

test_that("Success run 2", {
  columns = list("mpg","cyl")
  df2 = "Total no of columns in mtcars are mpg,cyl" 
  expect_message(zssay("Total no of columns in mtcars are", columns),df2)
})

test_that("only quoted pass",{
  expect_message(zssay("The values are"),"The values are" )
})
library(dplyr)
library(tidyr)
test_that("input error", {
  df1 = c(1,3,4)
  expect_error(zsutlcrun(df1,list("wdt"),list("wdt"),list("wdt")),"input must be a dataframe")
})

test_that("invalid column", {
  df1 = mtcars
  expect_message(zsutlcrun(df1,list("cylr"),list("hp"),list("mpg"),values_fn = sum),"Column `cylr` doesn't exist")
})

inp = data.frame(Name= c('A', 'I', 'A', 'I', 'E', 'E', 'J', NA, 'C', 'B', 'A'),
                 Class= c('X', 'Z', 'X', 'Z', 'Y', 'Y', 'Z', NA, 'X', NA, 'X'),
                 Marks= c(1, 9, NA, 7, 6, 5, NA, 4, 3, NA, 1),
                 Buckets= c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), stringsAsFactors = FALSE)

test_that("Success run", {
  oup = data.frame(Name= c('A','I','E','J',NA,'C','B'),
                   Marks_X= c(NA, NA, NA, NA, NA, 3, NA),
                   Marks_Z= c(NA, 16, NA, NA, NA, NA, NA),
                   Marks_Y= c(NA, NA, 11, NA, NA, NA, NA),
                   Marks_NA = c(NA, NA, NA, NA, 4, NA, NA), stringsAsFactors = FALSE)
  expect_equal(zsutlcrun(inp,"Name","Class",list("Marks"),values_fn = sum),oup)
})

# test_that("Success run value_as_suffix 'Y'", {
#   oup = data.frame(Name= c('A','I','E','J',NA,'C','B'),
#                    X_Marks= c(NA, NA, NA, NA, NA, 3, NA),
#                    Z_Marks= c(NA, 16, NA, NA, NA, NA, NA),
#                    Y_Marks= c(NA, NA, 11, NA, NA, NA, NA),
#                    NA_Marks = c(NA, NA, NA, NA, 4, NA, NA), stringsAsFactors = FALSE)
#   expect_equal(zsutlcrun(inp,"Name","Class",list("Marks"),values_fn = sum,value_as_suffix = 'Y'),oup)
# })
# 
# test_that("Success run value_as_suffix 'Y' fill na 0", {
#   oup = data.frame(Name= c('A','I','E','J',NA,'C','B'),
#                    X_Marks= c(NA, 0, 0, 0, 0, 3, 0),
#                    Z_Marks= c(0, 16, 0, NA, 0, 0, 0),
#                    Y_Marks= c(0, 0, 11, 0, 0, 0, 0),
#                    NA_Marks = c(0, 0, 0, 0, 4, 0, NA), stringsAsFactors = FALSE)
#   expect_equal(zsutlcrun(inp,"Name","Class",list("Marks"),values_fn = sum,value_as_suffix = 'Y',fill_na = 0),oup)
# })

test_that("Success run,count  fill na 0", {
  oup = data.frame(Name= c('A','I','E','J',NA,'C','B'),
                   Marks_X= c(3, 0, 0, 0, 0, 1, 0),
                   Marks_Z= c(0, 2, 0, 1, 0, 0, 0),
                   Marks_Y= c(0, 0, 2, 0, 0, 0, 0),
                   Marks_NA = c(0, 0, 0, 0, 1, 0, 1), stringsAsFactors = FALSE)
  expect_equal(zsutlcrun(inp,"Name","Class",list("Marks"),values_fn = length,fill_na = 0),oup)
})

test_that("Success run UTLCRUNX", {
  oup = data.frame(Buckets= c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                   Name_X= c('A','-','A','-','-','-','-','-','C','-','A'),
                   Name_Z= c('-','I','-','I','-','-','J','-','-','-','-'),
                   Name_Y= c('-','-','-','-','E','E','-','-','-','-','-'),
                   Name_NA = c('-','-','-','-','-','-','-',NA,'-','B','-'), stringsAsFactors = FALSE)
  expect_equal(zsutlcrun(inp,"Buckets","Class","Name",fill_na = "-"),oup)
})
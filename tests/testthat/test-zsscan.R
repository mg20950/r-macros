test_that("success run", {
  zsscan(code = "z__! = !",list(5,7,10))
  expect_equal(z__5, 5)
  expect_equal(z__7, 7)
  expect_equal(z__10, 10)
})

test_that("error check", {
  expect_message(zsscan(code = "z__! = !"),"argument ","value_list ","is missing, with no default")
})